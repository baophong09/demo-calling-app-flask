from opentok import OpenTok

from settings import OPENTOK_API_KEY, OPENTOK_API_SECRET


class OpenTokServices(object):
    def __new__(cls):
        return OpenTok(OPENTOK_API_KEY, OPENTOK_API_SECRET)

import firebase_admin
import string, random
import json
import requests
import time
from flask import Flask, request, jsonify, abort
from faker import Faker
from firebase_admin import db, credentials
from opentok import MediaModes, Roles

from services.opentok import OpenTokServices

app = Flask(__name__)

cred = credentials.Certificate("/home/dinhphong/Desktop/serviceAccountKey.json")  # firebase generate
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://test-calling-83e2b.firebaseio.com'
})

fake = Faker()

class NotificationSender(object):
    FCM_KEY = 'AAAArJcTsXg:APA91bGgiYUt6PrRYE2mb44Mb5C5AuxMXugVPGCTvG7JzQn-9Qk4R_36DKTwOt4SWfKSaqVl0_AQdW_8f_rFh0B9ih6DjqT9ZIoAfgUqAWk4F5lXEm5ba0bl1XCo8Hu-ve6djmvZRrwn'

    @staticmethod
    def send(data):
        headers = {
            'Authorization': "key={}".format(NotificationSender.FCM_KEY),
            'Content-Type': 'application/json'
        }

        requests.post('https://fcm.googleapis.com/fcm/send', json=data, headers=headers)
        return True


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/answer-call', methods=['POST'])
def cancel_call():
    data = json.loads(request.data.decode('utf-8'))
    caller_id = data.get('caller')
    receiver_id = data.get('receiver')

    if not caller_id or not receiver_id:
        print('error caller or receiver', caller_id, receiver_id)
        abort(400)

    opentok = OpenTokServices()
    session = opentok.create_session(media_mode=MediaModes.routed)
    session_id = session.session_id

    token = opentok.generate_token(
        session_id,
        role=Roles.publisher,
        expire_time=int(time.time()) + (60 * 60 * 24 * 20)
    )

    ref = db.reference('users/{}'.format(caller_id))
    caller = ref.get()
    caller_token = caller.get('token')
    notif_data = {
        "to": caller_token,
        "content_available": True,
        "data": {
            "sessionId": session_id,
            "token": token,
            "type": "ANSWER",
            "finish": True
        }
    }
    NotificationSender.send(notif_data)

    # receiver token
    token = opentok.generate_token(
        session_id,
        role=Roles.publisher,
        expire_time=int(time.time()) + (60 * 60 * 24 * 20)
    )

    return jsonify({
        "sessionId": session_id,
        "token": token
    })


@app.route('/<string:user_id>/call', methods=['POST'])
def call(user_id):
    data = json.loads(request.data.decode('utf-8'))
    caller = data.get('userId')

    if not caller:
        abort(400, jsonify({"error": "Caller not found"}))

    ref = db.reference('users/{}'.format(user_id))
    user_receive = ref.get()

    if not user_receive:
        abort(400, jsonify({"error": "User token not found"}))

    token = user_receive.get('token')

    if not token:
        abort(400, jsonify({"error": "User token not found"}))

    notif_data = {
        "to": token,
        "content_available": True,
        "data": {
            "fromId": caller,
            "fromLogin": fake.name(),
            "fromImage": "http://i.pravatar.cc/200?u={}".format(''.join(random.choices(string.ascii_uppercase + string.digits, k=6))),
            "type": "CALL",
            "finish": True
        }
    }

    NotificationSender.send(notif_data)

    return jsonify({
        "name": fake.name(),
        "picture": "http://i.pravatar.cc/200?u={}".format(''.join(random.choices(string.ascii_uppercase + string.digits, k=6)))
    })
